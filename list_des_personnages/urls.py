


from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_personnage),
    path('add_new_personnage', views.ajouter_un_personnage)
]