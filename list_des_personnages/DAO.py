import json 
from django.conf import settings
import os 


class personnagesDAO:
    liste = None

    @staticmethod
    def get_liste_des_personnages():
        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "data.json"), 'r') as fichier:
            personnagesDAO.liste= json.loads(fichier.read())
        
        return personnagesDAO.liste
    
    @staticmethod
    def add_personnage(name, characteristics, image):
        image_name = str(name) + ".png"
        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "images", image_name) , 'wb+') as dest:
            for chunk in image.chunks():
                dest.write(chunk)
        
        personnage = {"name": name, "characteristics":characteristics, "photo":"images/"+image_name}
        
        db_content = personnagesDAO.get_liste_des_personnages()

        db_content.append(personnage)

        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "data.json"), 'w') as fichier:
            fichier.write(json.dumps(db_content))