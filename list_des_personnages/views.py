from django.shortcuts import render

from .DAO import personnagesDAO

# Create your views here.

from django.http import JsonResponse

def list_personnage(request):
    return render(request, "list/liste_des_personnages.html", context = {"personnages": personnagesDAO.get_liste_des_personnages()})

def ajouter_un_personnage(request):
    image = request.FILES['image_du_personnage']

    name = request.POST.get("nom_du_personnage")

    characteristics = request.POST.get("characteristics")

    if None in (name, characteristics):
        return JsonResponse({"status":"failure: not enough args: " + str((image, name, characteristics))})

    try:
        return JsonResponse({"status": personnagesDAO.add_personnage(name, characteristics, image)})

    except Exception as err:
        return JsonResponse({"status":"failure"})


