from django.apps import AppConfig


class ListDesPersonnagesConfig(AppConfig):
    name = 'list_des_personnages'
