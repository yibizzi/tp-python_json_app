FROM python:3.6

ARG PATH

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN  pip install --default-timeout=100 -r requirements.txt

RUN adduser --disabled-password myuser
USER myuser

WORKDIR /code/personnages
CMD gunicorn --bind 0.0.0.0:$PORT wsgi